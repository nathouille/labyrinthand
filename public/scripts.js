  // parametres handtrack
  let videoActivee = false;
  let modeleTracking = null;
  const parametresModeleTracking = {
    flipHorizontal: true,   // flip e.g for video  
    maxNumBoxes: 20,        // maximum number of boxes to detect
    iouThreshold: 0.5,      // ioU threshold for non-max suppression
    scoreThreshold: 0.6,    // confidence threshold for predictions.
  }
  
  
  
  window.addEventListener("DOMContentLoaded", (event) => {
    
    let scenePhysique=Environnement3D.creerScene();
    
    
    // récuperation elements du DOM
    let trackButton= document.getElementById("trackbutton");
    trackButton.disabled = true;
    let updateNote = document.getElementById("updatenote");
    const video = document.getElementById("myvideo");
    var videoW,videoH;
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");    
    
    trackButton.addEventListener("click", function(){
      if (!videoActivee) {
        updateNote.innerText = "Lancement de la camera"
        startVideo();
      } else {
        updateNote.innerText = "Arreter la camera"
        handTrack.stopVideo(video)
        videoActivee = false;
        updateNote.innerText = "Camera arretee"
      }
    });
    
    // Load the model.
    handTrack.load(parametresModeleTracking).then(lmodel => {
      // detect objects in the image.
      modeleTracking = lmodel
      updateNote.innerText = "Modele chargé"
      trackButton.disabled = false
    });
    
    function startVideo() {
      handTrack.startVideo(video).then(function (status) {
        if (status) {
          updateNote.innerText = "Video démarrée. Lancement du suivi"
          videoActivee = true
          videoW=video.width;
          videoH=video.height;
          runDetection()
        } else {
          updateNote.innerText = "Lancer la caméra SVP"
        }
      });
    }
    
    function runDetection() {
      modeleTracking.detect(video).then(predictions => {
        var pred2=predictions;
        modeleTracking.renderPredictions(predictions, canvas, context, video); // affiche la bounding box et la confitance
        for(let i=0;i<pred2.length;i++){
          if(pred2[i].class==1){
            let bbox=pred2[i].bbox;
            const centerX=bbox[0]+bbox[2]/2;
            const centerY=bbox[1]+bbox[3]/2;
            scenePhysique.bougerLaMain(centerX/videoW,centerY/videoH);
          }    
        }
        
        if (videoActivee) {
          requestAnimationFrame(runDetection);
        }
      });
    }
    
  });
  
  var Environnement3D = Environnement3D || {};
  
  Environnement3D.creerScene = function() {
    var Engine = Matter.Engine,
    Render = Matter.Render,
    Runner = Matter.Runner,
    Composites = Matter.Composites,
    Events = Matter.Events,
    Constraint = Matter.Constraint,
    MouseConstraint = Matter.MouseConstraint,
    Mouse = Matter.Mouse,
    World = Matter.World,
    Bodies = Matter.Bodies;
    
    // create engine
    var engine = Engine.create(),
    world = engine.world;
    
    // create renderer
    var render = Render.create({
      element: document.getElementById('matter'),
      engine: engine,
      options: {
        width: 800,
        height: 600,
        wireframes: false
      }
    });
    
    Render.run(render);
    
    // create runner
    var runner = Runner.create();
    Runner.run(runner, engine);

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////

    // add bodies
    var mur1 = Bodies.rectangle(400, 600, 1000, 5, { isStatic: true }),
    rondOptions = { density: 0.004 },
    rond = Bodies.circle(50, 450, 20, rondOptions),
    ancre = { x: 170, y: 450 };


  var pyramid = Composites.pyramid(500, 300, 9, 10, 0, 0, function (x, y) {
    return Bodies.rectangle(x, y, 25, 40);
  });

//  Création des mur du labyrinthe
var mur2 = Bodies.rectangle(800, 300, 5, 1000, { isStatic: true });
var mur3 = Bodies.rectangle(400, 0, 1000, 5, { isStatic: true });
var mur4 = Bodies.rectangle(0, 300, 5, 1000, { isStatic: true });
var mur5 = Bodies.rectangle(100, 550, 5, 100, { isStatic: true });
var mur6 = Bodies.rectangle(100, 400, 200, 5, { isStatic: true });
var mur7 = Bodies.rectangle(250, 300, 500, 5, { isStatic: true });
var mur8 = Bodies.rectangle(100, 100, 5, 200, { isStatic: true });
var mur9 = Bodies.rectangle(200, 200, 5, 200, { isStatic: true });
var mur10 = Bodies.rectangle(100, 100, 5, 200, { isStatic: true });
var mur11 = Bodies.rectangle(350, 200, 100, 5, { isStatic: true });
var mur12 = Bodies.rectangle(400, 150, 5, 100, { isStatic: true });
var mur13 = Bodies.rectangle(300, 100, 5, 200, { isStatic: true });
var mur14 = Bodies.rectangle(500, 50, 5, 100, { isStatic: true });
var mur15 = Bodies.rectangle(600, 100, 200, 5, { isStatic: true });
var mur16 = Bodies.rectangle(700, 150, 5, 100, { isStatic: true });
var mur17 = Bodies.rectangle(500, 300, 5, 200, { isStatic: true });
var mur18 = Bodies.rectangle(550, 200, 100, 5, { isStatic: true });
var mur19 = Bodies.rectangle(600, 250, 5, 100, { isStatic: true });
var mur20 = Bodies.rectangle(650, 300, 100, 5, { isStatic: true });
var mur21 = Bodies.rectangle(700, 400, 5, 200, { isStatic: true });
var mur22 = Bodies.rectangle(400, 500, 5, 200, { isStatic: true });
var mur23 = Bodies.rectangle(500, 500, 200, 5, { isStatic: true });
var mur24 = Bodies.rectangle(600, 450, 5, 100, { isStatic: true });
var mur25 = Bodies.rectangle(300, 400, 5, 200, { isStatic: true });
var mur26 = Bodies.rectangle(250, 500, 100, 5, { isStatic: true });

  World.add(engine.world, [rond, mur1, mur2, mur3, mur4, mur5, mur6, mur7,
     mur8, mur9, mur10, mur11, mur12, mur13, mur14, mur15, mur16, mur17, mur18,
      mur19, mur20, mur21, mur22, mur23, mur24, mur25, mur26]);

  Events.on(engine, "variable", function () {
    if (
      mouseConstraint.mouse.button === -1 &&
      (rond.position.x > 190 || rond.position.y < 430)
    ) {
      rond = Bodies.circle(170, 450, 20, rondOptions);
      World.add(engine.world, rond);
    }
  });

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////

    // add mouse control
    var mouse = Mouse.create(render.canvas),
    mouseConstraint = MouseConstraint.create(engine, {
      mouse: mouse,
      constraint: {
        stiffness: 0.98,
        render: {
          visible: false
        }
      }
    });
    
    World.add(world, mouseConstraint);
    
    // keep the mouse in sync with rendering
    render.mouse = mouse;
    
    // fit the render viewport to the scene
    Render.lookAt(render, {
      min: { x: 0, y: 0 },
      max: { x: 800, y: 600 }
    });
    
    // context for MatterTools.Demo
    return {
      engine: engine,
      runner: runner,
      render: render,
      canvas: render.canvas,
      stop: function() {
        Matter.Render.stop(render);
        Matter.Runner.stop(runner);
      },
      bougerLaMain(x,y){  
        Matter.Body.setVelocity(rond, { x: (x - 0.5) * 10, y: (y - 0.5) * 10 });
      }
    };
  };